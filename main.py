import NGram
import random_gen as rg
if __name__ == '__main__':
    """
    To test this, first create a corpus. Other options include "crime" and
    "history" as options. We pass the corpus as an argument to the ngram
    generator.

    example code:
    corpus = NGram.Corpus("children")
    """
    print "Creating corpus."
    corpus = NGram.Corpus("children")
    print "Created corpus"

    """
    Then we create a RandomGenerator Object. This is the onject on which we can
    call the generate() function over and over again. FOr details of this
    function, look up the documentation for generate() in random_gen, or type
    help(random_gen) at the terminal after running import random_gen"""
    print "Creating generator"
    generator = rg.RandomGenerator(corpus)
    print "Created generator"

    print "Generating 10 unigrams"
    for i in range(10):
        print generator.generate(1)

    print "Generating 10 bigrams"
    for i in range(10):
        print generator.generate(2)

    print "This seems to run"
