import NGram
import string
import numpy as np 

class RandomGenerator():
    def __init__(self, corpus):
        """The corpus is a dictionary of n-word sequences that map to the
        probabilites of those n-sequences existing if the (n-1) first words are
        already in a sequence. 
        """
        self.corpus = corpus
        self.terminator = ['.','!','?']

    def generate(self,n = 1, prefix = ''):
        """generates or completes a sentence 
        i is the length of the ngram.
        a bigram would have i=2, etc
        Only supporting uni- and bi-grams right now. """
        current = prefix
        result = [current] if current else [] 
        probs = self.corpus.getNgramLists(n)
        if(n==1):
            while(current not in self.terminator):
                pairs = probs
                words = []
                probabilities = []
                for i in pairs:
                    words.append(i[0])
                    probabilities.append(i[1])
                current = np.random.choice(words, 1, probabilities)[0]
                result+=[current]


        elif(n==2):
            if(current not in probs):
                current = ''
            while(current not in self.terminator):
                pairs = probs[current]
                words = []
                probabilities = []
                for i in pairs:
                    words.append(i[0])
                    probabilities.append(i[1])
                current = np.random.choice(words, 1, probabilities)[0]
                result+=[current]
        else:
            print "FAILURE"
            return
        return self.printSentence(result)
    
    def printSentence(self,words):
        words[0] = string.upper(words[0][0])+words[0][1:]
        toreturn = string.join(words[:-1], ' ')
        toreturn += words[-1]

        return toreturn
