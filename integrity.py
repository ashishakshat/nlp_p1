"""Prints lists of words the sum of whose probability lists comes to be
to far away from 1. Not many words, but still a little scary."""
import NGram as N

if __name__ == '__main__':
    for genre in ['children', 'crime', 'history']:
        print genre
        child = N.Corpus(genre)
        bigramList = child.getNgramLists(2)#This gives a bigram dictionary
        for key,value in bigramList.iteritems():
            val = sum([i[1] for i in value])
            if (not (0.99999 < val < 1.00001)):
                print key, val
