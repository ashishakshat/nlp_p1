import smoothing
import os


class GenreClassifier():

    def __init__ (self):
       self.testGenre = {}
       self.testFileNames = []
       self.mapTestGenre(["children","crime","history"],self.testGenre,self.testFileNames)
       self.predictedGenres = self.classify(self.testFileNames)
    
       
    def mapTestGenre(self,genres,testGenre,testFileNames):
      for genre in genres:
         directory = "books/test_books/"+genre+"/"
         for subdir, dirs, files in os.walk(directory):
            for f in files: 
                testGenre[(directory+f)] = genre
                testFileNames.append((directory+f))

    def classify(self,testFileNames):
        predictedGenres = {}
        genres = ["children","crime","history"]
        for f in testFileNames:
            print f
            x = smoothing.GoodTuring("children",f)
            p1 = x.getPerplexity()
            y = smoothing.GoodTuring("crime",f)
            p2 = y.getPerplexity()
            z = smoothing.GoodTuring("history",f)
            p3 = z.getPerplexity()
            perplexities = [p1,p2,p3]
            print perplexities
            perps = [x[1] for x in perplexities]
            i = perps.index(min(perps))
            genre = genres[i]
            predictedGenres[f] = genre
            
        return predictedGenres

    def getClassifications(self):
        return self.predictedGenres
