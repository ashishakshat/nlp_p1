import NGram
from math import log
from math import exp
from nltk.tokenize import RegexpTokenizer
import os

class GoodTuring(): 

    def __init__(self,genre,filename):
        self.corpus = NGram.Corpus(genre)
        counts = self.corpus.getCounts()
        #not a deepcopy, but we don't really care
        unigram_counts = counts.get("1")
        bigram_counts = counts.get("2")

        self.tokenizer = RegexpTokenizer('\w+|\$[\d\.]+|\S+')
        self.uni_test_counts = {}
        self.bi_test_counts = {} 
        self.getTestTokens(filename, self.uni_test_counts,self.bi_test_counts)
        word_count = float(sum([value for key,value in
            self.uni_test_counts.iteritems()]))
        self.correctUniTokens(unigram_counts,self.uni_test_counts)
        self.correctBiTokens(bigram_counts,self.bi_test_counts)
        new_uni_count = self.correctCounts(unigram_counts)
        new_bi_count = self.correctCounts(bigram_counts)
        #NEHA ADD TILL HERE

        #Now we have good uni/bigram counts
        self.perplexity = self.setPerplexity(new_uni_count, new_bi_count,
                filename, word_count) 

   
    def setPerplexity(self,unigram_counts,bigram_counts,filename, word_count):
        openfile = open(filename, 'r')
        lines = openfile.readlines()
        filetext = ' '.join(lines)
        
        tokens = self.tokenizer.tokenize(filetext)
        uni_perp = 0.0
        bi_perp = 0.0
        totals = {}
        for key,value in bigram_counts.iteritems():
            if key[0] not in totals:
                totals[key[0]] = value
            else:
                totals[key[0]] += value
        for index,word in enumerate(tokens[:-1]):
            #Goes through each word. KyalKyulate now.
            unikey = word
            bikey = (word,tokens[index+1])
            try:
                bi_perp += log(bigram_counts[bikey]/float(totals[bikey[0]]))
            except:
                print bikey
                print bigram_counts[bikey]
            try:
                uni_perp += log(unigram_counts[unikey]/word_count)
            except:
                print unikey 
                print unigram_counts[unikey]
        uni_perplexity = exp(-uni_perp/word_count)
        bi_perplexity = exp(-bi_perp/word_count)

        return (uni_perplexity, bi_perplexity)
    
    def getPerplexity(self):
        return self.perplexity

    def getTestTokens(self,filename,unigram_counts,bigram_counts):
        uni_tokens = []
        bi_tokens = []
        openfile = open(filename,'r')
        lines = openfile.readlines()
        self.corpus.getUniBigrams(lines,unigram_counts,bigram_counts)

    def correctUniTokens(self,unigram_counts,uni_test_counts):
         for key,value in uni_test_counts.iteritems():
               if (key not in unigram_counts):
                   unigram_counts[key] = 0

    def correctBiTokens(self,bigram_counts,bi_test_counts):
         for key,value in bi_test_counts.iteritems():
               if (key not in bigram_counts):
                   bigram_counts[key] = 0 

    def correctCounts(self,dictionary):
         j = min(dictionary.values())
         i = max(dictionary.values())
         counts = {key:0 for key in range(j,i+i)}
         for key,value in dictionary.iteritems():
              counts[value] += 1
         newDictionary = {}
         for key,value in dictionary.iteritems():
              Nc = counts[value]
              Nc1 = counts[value+1]
              if(value <= 5):
                  newValue = ((value+1) * Nc1)/float(Nc) 
                  newDictionary[key] = newValue
              else:
                  newDictionary[key] = value
         return newDictionary




