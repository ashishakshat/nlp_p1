from nltk.tokenize import RegexpTokenizer
from collections import Counter
import os 

class Corpus():
  ''' probabilities --> {"1":{wd1:p1,wd2:p2...},
  "2":{(wd1,wd2):p1,(wd3,wd4):p2,...}}'''
  probabilities = {} 

  def __init__(self,genre):
    ''' Updates 'probabilities' to be a dictionary of unigram and bigram 
    probabilities of words.
    genre - is either "children","crime" or "history"
    '''
    unigrams_dict = {}
    bigrams_dict = {}
    genre_dicts = {}
    genre_bigrams ={}

    childrens_dir = "books/train_books/" + genre + "/"
    self.walkThroughBooks(childrens_dir,genre_dicts, genre_bigrams,
    unigrams_dict,bigrams_dict)

    self.probabilities = {"1":unigrams_dict,"2":bigrams_dict}
    self.counts = {"1": genre_dicts, "2": genre_bigrams}
  
  def walkThroughBooks(self,main_dir,dictionary,bigrams,unigrams_dict,bigrams_dict):
    ''' Walk through a given directory of books and collects unigram and bigram
    from all text files in that folder '''
    for subdir, dirs, files in os.walk(main_dir):
          for f in files :
            if f[0] != '.':
              filename = main_dir + f 
              openfile = open(filename,'r')
              lines = openfile.readlines()
              self.getUniBigrams(lines,dictionary,bigrams)
              
              self.setProbabilities(unigrams_dict,bigrams_dict,
                dictionary,bigrams)

  def getProbabilities(self):
    '''Returns dictionary with final unigram and bigram probabilities'''
    return self.probabilities

 
  def getCounts(self):
    '''Return dictionary with final unigram and bigram counts'''
    return self.counts


  def getNgramLists(self, n):
    masterDict = self.getProbabilities()
    unigram_dict = masterDict['1']
    
    unigram_list = []
    for key,value in unigram_dict.iteritems():
        unigram_list.append((key,value))

    if(n==1):
        return unigram_list

    bigram_dicts = masterDict['2']
    bigram_lists = {}
    for key,value in bigram_dicts.iteritems():
        word1 = key[0]
        word2 = key[1]
        prob = value
        if word1 in bigram_lists:
            bigram_lists[word1].append((word2, prob))
        else:
            bigram_lists[word1] = [(word2, prob)]
       
    bigram_lists['']=unigram_list
    
    return bigram_lists 

  def tokenize(self,line):
    '''Tokenizes a given string
    Returns - generated list of tokens'''
    tokenizer = RegexpTokenizer('\w+|\$[\d\.]+|\S+')
    tokens = tokenizer.tokenize(line)
    return tokens
  
  def get_bigrams(self,tokens):
    '''Generates bigrams from a given set of tokens in order 
    Returns - List of bigrams '''
    bigrams = []
    #TODO: issue is here, index is not consistent.
    #for x in tokens:
        #if (tokens.index(x)+1) < len(tokens):
           #bigrams.append((x,tokens[tokens.index(x)+1]))
    for (index, word) in enumerate(tokens[:-1]):
        bigrams.append((word, tokens[index+1]))
    return bigrams
  
  def getUniBigrams(self,lines,dictionary,bigrams_dict):
        '''Gets unigram and bigram tokens from given lines and 
        calculates counts for them '''
        last_token = ""
        for line in lines:
            tokens = self.tokenize(line)
            if tokens != []: 
                  bigrams = self.get_bigrams(tokens)
                  if (last_token != ""):
                     bigrams.append((last_token,tokens[0]))
                  last_token = tokens[len(tokens)-1]
                  for token in tokens:
                    if (token in dictionary):
                       dictionary[token] += 1
                    else:
                        dictionary[token] = 1
                  for bigram in bigrams:
                    if (bigram in bigrams_dict):
                         bigrams_dict[bigram] += 1
                    else:
                         bigrams_dict[bigram] = 1


  def setProbabilities(self,uni_prob,bi_prob,unigrams,bigrams):
        ''' Calculates probabilities for each unigram and bigram and 
        stores them in dictionaries '''
        no_of_tokens = sum(unigrams.values())
        for unigram in unigrams:
            uni_prob[unigram] = float(unigrams.get(unigram))/float(no_of_tokens)
        for bigram in bigrams:
            if (type(bigram) == tuple):
              bi_prob[bigram] = float(bigrams.get(bigram))/float(unigrams.get(bigram[0]))



              
          
